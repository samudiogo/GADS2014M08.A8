﻿namespace GADS2014M08.A8.Domain.Entities
{
    public class Endereco
    {
        public int Id { get; set; }
        public string Longradouro { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string Cep { get; set; }

        public virtual Aluno Aluno { get; set; }
    }
}