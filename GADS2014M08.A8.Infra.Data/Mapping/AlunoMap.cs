﻿using GADS2014M08.A8.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace GADS2014M08.A8.Infra.Data.Mapping
{
    public class AlunoMap : EntityTypeConfiguration<Aluno>
    {
        public AlunoMap()
        {
            ToTable("Alunos");
            HasKey(t => t.Id);
            HasOptional(a => a.Endereco)
                .WithRequired(ad => ad.Aluno);

        }
    }
}
