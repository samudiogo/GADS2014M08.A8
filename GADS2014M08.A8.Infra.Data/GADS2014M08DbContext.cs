﻿using GADS2014M08.A8.Domain.Entities;
using GADS2014M08.A8.Infra.Data.Mapping;
using System.Data.Entity;

namespace GADS2014M08.A8.Infra.Data
{
    public class GADS2014M08DbContext:DbContext
    {
        public IDbSet<Aluno> Alunos { get; set; }

        public GADS2014M08DbContext():base("GADS2014M08DbContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AlunoMap());
        }
    }
}
