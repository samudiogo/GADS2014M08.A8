﻿using GADS2014M08.A8.Domain.Entities;
using GADS2014M08.A8.Infra.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GADS2014M08.A8.Presentation.DesktopApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GADS2014M08DbContext _ctx = new GADS2014M08DbContext();

        public MainWindow()
        {
            InitializeComponent();

            cbxEstado.ItemsSource = ListaEstado();
        }

        private static List<string> ListaEstado()
        {
            return new List<string> {
                "Acre"
                ,"Alagoas"
                ,"Amapá"
                ,"Amazonas"
                ,"Bahia"
                ,"Ceara"
                ,"Distrito Federal"
                ,"Espírito Santo"
                ,"Goiás"
                ,"Maranhão"
                ,"Mato Grosso"
                ,"Mato Grosso do Sul"
                ,"Minas Gerais"
                ,"Pará"
                ,"Paraíba"
                ,"Paraná"
                ,"Pernambuco"
                ,"Piauí"
                ,"Rio de Janeiro"
                ,"Rio Grande do Norte"
                ,"Rio Grande do Sul"
                ,"Rondônia"
                ,"Roraima"
                ,"Santa Catarina"
                ,"São Paulo"
                ,"Sergipe"
                ,"Tocantins"
            };
        }

        private void btnCadastrar_Click(object sender, RoutedEventArgs e)
        {
            var rdbSexo = string.Empty;

            if (rbsexoF.IsChecked.HasValue)
                rdbSexo = rbsexoF.Content.ToString();
            else
                rdbSexo = rbSexoM.Content.ToString();
            
            var aluno = new Aluno
            {
                Nome = nome.Text,
                DataNascimento = DateTime.Parse(dataNascimento.Text),
                Sexo = char.Parse(rdbSexo),
                Email = email.Text,
                Senha = senha.Password,
                TelefoneCelular = telefoneCelular.Text,
                TelefoneFixo = telefoneFixo.Text,
                Endereco = new Endereco
                {
                    Bairro = bairro.Text,
                    Longradouro = endereco.Text,
                    Cidade = cidade.Text,
                    Estado = cbxEstado.Text,
                    Cep = cep.Text
                }
            };

            _ctx.Alunos.Add(aluno);
            if (_ctx.SaveChanges() > 0)
                MessageBox.Show("Cadastrado com sucesso");
            
        }
    }
}
